all: plotMng 
	
plotMng:
	mkdir -p ./build
	mkdir -p ./build/plotMng
	mkdir -p ./bin
	mkdir -p ./bin/plotMng
	cmake -D CMAKE_BUILD_TYPE=Release -B./build/plotMng -H./src/plotMng
	make -C ./build/plotMng -j `nproc` VERBOSE=1
	
