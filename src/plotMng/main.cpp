#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include "boost/program_options.hpp" 
#include "boost/filesystem.hpp"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>


#include "OptionPrinter.hpp"
#include <set>
#include <stdexcept>



struct record
    {
    std::list<std::string> parameter_names;
    std::list<double> parameter_values;
    std::string data_name;
    std::vector<double> data;
    };

class plot_data_archiver
    {
    public:
        plot_data_archiver(){};
        ~plot_data_archiver(){};
        void serialize(std::ostream& st,const std::list<record>& data)
            {
            int int_tmp;
            for(auto it=data.begin();it!=data.end();++it)
                {
                int_tmp=it->parameter_names.size();
                st.write((char*)&(int_tmp),4);
                auto it2=it->parameter_names.begin();
                for(auto it1=it->parameter_values.begin();it1!=it->parameter_values.end();++it1)
                    {
                    int_tmp=it2->size();
                    st.write((char*)&(int_tmp),4);
                    st.write(it2->data(),int_tmp);
                    st.write((char*)&(*it1),8);
                    ++it2;
                    }
                int_tmp=it->data_name.size();
                st.write((char*)&(int_tmp),4);
                st.write(it->data_name.data(),int_tmp);
                int_tmp=it->data.size();
                st.write((char*)&(int_tmp),4);
                for(auto it1=it->data.begin();it1!=it->data.end();++it1)
                    {
                    st.write((char*)&(*it1),8);
                    }
                }
            }
        void deserialize(std::istream& st,std::list<record>& data)
            {
            int int_tmp,int_tmp1;
            int current_string_length=1; 
            char* tmp_string=new char[1];
            while(!st.eof())
                {
                st.read((char*)&(int_tmp),4);
                data.push_back(record());
                for(int count1=0;count1<int_tmp;++count1)
                    {
                    st.read((char*)&(int_tmp1),4);
                    if(int_tmp1>current_string_length)
                        {
                        current_string_length=int_tmp1;
                        delete [] tmp_string;
                        tmp_string=new char[current_string_length];
                        }
                    st.read(tmp_string,int_tmp1);
                    data.back().parameter_names.push_back(std::string(tmp_string,int_tmp1));
                    data.back().parameter_values.push_back(0.0);
                    st.read((char*)&(data.back().parameter_values.back()),8);
                    }
                st.read((char*)&(int_tmp),4);
                //std::cout<<int_tmp<<std::endl;
                if(int_tmp>current_string_length)
                    {
                    current_string_length=int_tmp;
                    delete [] tmp_string;
                    tmp_string=new char[current_string_length];
                    }
                st.read(tmp_string,int_tmp);
                data.back().data_name=std::string(tmp_string,int_tmp);
                st.read((char*)&(int_tmp),4);
                //std::cout<<int_tmp<<std::endl;
                data.back().data.resize(int_tmp);
                st.read((char*)data.back().data.data(),8*int_tmp);
                st.peek();
                }
            }
    };

class plot_data
    {
        
        class bad_directory_error:public std::runtime_error
            {
                std::string dir;
            public:
                bad_directory_error(const std::string& in):runtime_error(""),dir(in)
                    {
                    
                    }
                    
                virtual ~bad_directory_error()
                    {
                    
                    }
                virtual const char* what() const noexcept
                    {
                    return ("bad dirrectory: "+dir).c_str();
                    }
            };
        
        class cubic_spline
            {
            private:
            // Структура, описывающая сплайн на каждом сегменте сетки
                struct spline_tuple
                    {
                    double a, b, c, d;
                    };

                std::vector<spline_tuple> splines; // Сплайн
                std::vector<double> base_abscissa;

                class my_invalid_argument:public std::invalid_argument
                    {
                    public:
                        my_invalid_argument():invalid_argument("")
                            {
                            
                            }
                            
                        virtual ~my_invalid_argument()
                            {
                            
                            }
                        virtual const char* what() const noexcept
                            {
                            return "invalid argument: can't create splines";
                            }
                    };
                
                
            public:
                cubic_spline(const std::vector<double> x, const std::vector<double> y) //конструктор
                    {
                    if(x.size()<3 || x.size()!=y.size())
                        throw my_invalid_argument();
                    splines.resize(x.size());
                    base_abscissa.resize(x.size());
                    for (std::size_t i = 0; i < x.size(); ++i)
                        {
                        base_abscissa[i] = x[i];
                        splines[i].a = y[i];
                        }
                    splines[0].c = 0.;
                    // Решение СЛАУ относительно коэффициентов сплайнов c[i] методом прогонки для трехдиагональных матриц
                    // Вычисление прогоночных коэффициентов - прямой ход метода прогонки
                    std::vector<double> alpha(x.size()-1);
                    std::vector<double> beta(x.size()-1);
                    double A, B, C, F, h_i, h_i1, z;
                    alpha[0] = beta[0] = 0.;
                    for (std::size_t i = 1; i <x.size()  - 1; ++i)
                        {
                        h_i = x[i] - x[i - 1], h_i1 = x[i + 1] - x[i];
                        A = h_i;
                        C = 2. * (h_i + h_i1);
                        B = h_i1;
                        F = 6. * ((y[i + 1] - y[i]) / h_i1 - (y[i] - y[i - 1]) / h_i);
                        z = (A * alpha[i - 1] + C);
                        alpha[i] = -B / z;
                        beta[i] = (F - A * beta[i - 1]) / z;
                        }
                    #pragma GCC diagnostic push
                    #pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
                    splines[x.size() - 1].c = (F - A * beta[x.size() - 2]) / (C + A * alpha[x.size() - 2]);
                    #pragma GCC diagnostic pop
                    
                    // Нахождение решения - обратный ход метода прогонки
                    for (std::size_t i = x.size() - 2; i > 0; --i)
                        splines[i].c = alpha[i] * splines[i + 1].c + beta[i];
                    
                    for (std::size_t i = x.size() - 1; i > 0; --i)
                        {
                        double h_i = x[i] - x[i - 1];
                        splines[i].d = (splines[i].c - splines[i - 1].c) / h_i;
                        splines[i].b = h_i * (2. * splines[i].c + splines[i - 1].c) / 6. + (y[i] - y[i - 1]) / h_i;
                        }
                    }
                ~cubic_spline()//деструктор
                    {
                    }

                                
                // Вычисление значения интерполированной функции в произвольной точке
                double operator()(const double& x) const
                    {
                    unsigned int id;
                    if (x <= base_abscissa[0]) // Если x меньше точки сетки x[0] - пользуемся первым эл-том массива
                        id = 1;
                    else if (x >= base_abscissa[splines.size() - 1]) // Если x больше точки сетки x[n - 1] - пользуемся последним эл-том массива
                        id = splines.size() - 1;
                    else // Иначе x лежит между граничными точками сетки - производим бинарный поиск нужного эл-та массива
                    {
                        std::size_t i = 0, j = splines.size() - 1;
                        while (i + 1 < j)
                        {
                            std::size_t k = i + (j - i) / 2;
                            if (x <= base_abscissa[k])
                                j = k;
                            else
                                i = k;
                        }
                        id = j;
                    }
                    const spline_tuple& s=splines[id];
                    const double & abs=base_abscissa[id];
                    double dx = (x - abs);
                    return s.a + (s.b + (s.c / 2. + s.d * dx / 6.) * dx) * dx; // Вычисляем значение сплайна в заданной точке.
                    }
                const std::vector<double>& get_base_absscissa()const
                    {
                    return base_abscissa;
                    }
            };
        struct plot_data_record
            {
            std::list<std::string> parameter_names;
            std::list<double> parameter_values;
            std::string data_name;
            cubic_spline inter;
            plot_data_record(const std::vector<double> x, const std::vector<double> y):inter(x,y)
                {
                
                }
            };
        
        std::string my_dir;
        std::vector<plot_data_record> plot_data_records_vector;
    public:
        plot_data(std::string dir):my_dir(dir)
            {
            plot_data_archiver archiver;
            std::list<record> records;
            if ( !boost::filesystem::exists( my_dir+"data.bin" ) )
                throw bad_directory_error(my_dir);
            std::ifstream ifs;
            ifs.open(my_dir+"data.bin",std::ifstream::binary);
            archiver.deserialize(ifs,records);
            ifs.close();
            unsigned int count=0;
            for(auto it_res=records.begin();it_res!=records.end();++it_res,++count)
                {
                std::vector<double> x_tmp(it_res->data.size());
                double dx=1.0/double(it_res->data.size()-1);
                auto it=x_tmp.begin();
                auto it1=it;
                *it=0.0;
                ++it;
                while(it!=x_tmp.end())
                    {
                    *it=*it1+dx;
                    ++it;
                    ++it1;
                    }
                plot_data_records_vector.push_back(plot_data_record(x_tmp,it_res->data));
                plot_data_records_vector.back().parameter_names=it_res->parameter_names;
                plot_data_records_vector.back().parameter_values=it_res->parameter_values;
                plot_data_records_vector.back().data_name=it_res->data_name;
                }
            }
        ~plot_data()
            {
            }
		void print_data_information(std::ostream& st)
			{
			st<<"Plot dir: "<<my_dir<<std::endl;
			for(auto it=plot_data_records_vector.begin();it!=plot_data_records_vector.end();++it)
				{
				st<<"**************************************************\n";
				st<<"Parameters: ";
				auto it2=it->parameter_values.begin();
				for(auto it1=it->parameter_names.begin();it1!=it->parameter_names.end();++it1,++it2)
					{
					st<<*it1<<" = "<<*it2<<"; ";
					}
				st<<std::endl;
				st<<"Data name: "<<it->data_name<<std::endl;
				}
			}
		void build_data_for_gnuplot(void)
			{
			if ( !boost::filesystem::exists( my_dir+"data.ini" ) )
                throw bad_directory_error(my_dir);
			boost::property_tree::ptree pt;
			boost::property_tree::ini_parser::read_ini(my_dir+"data.ini", pt);
			
			for(auto it=pt.begin();it!=pt.end();++it)
				{
				std::string file_name=it->first;
				unsigned int x_id=it->second.get<unsigned int>("x");
                unsigned int y_id=it->second.get<unsigned int>("y");
                std::ofstream ofs;
                ofs.open(my_dir+file_name,std::ofstream::out | std::ofstream::trunc);
                std::vector<double> x;
                if(plot_data_records_vector[x_id].inter.get_base_absscissa().size()>1000)
                    {
                    double dx=1.0/999.0;
                    x.resize(1000);
                    x[0]=0.0;
                    auto it1=x.begin();
                    auto it2=x.begin();
                    ++it2;
                    while(it2!=x.end())
                        {
                        *it2=*it1+dx;
                        ++it1;
                        ++it2;
                        }
                    }
                else
                    {
                    x=plot_data_records_vector[x_id].inter.get_base_absscissa();
                    }
                for(auto it1=x.begin();it1!=x.end();++it1)
                    {
                    ofs<<plot_data_records_vector[x_id].inter(*it1);
                    ofs<<" ";
                    ofs<<plot_data_records_vector[y_id].inter(*it1);
                    ofs<<std::endl;
                    }
                ofs.close();
                }
			}
    };


int main(int argc,char ** argv)
    {
    std::vector<std::string> dirs;
    
    std::string appName = boost::filesystem::basename(argv[0]);
    boost::program_options::options_description GenOpt("Generic options");
    GenOpt.add_options()
            ("help,h","produce this message")
            ("information,i","print information about plot data")
            ("build,b","build figure")
            ;
    boost::program_options::options_description HidOpt("Hiden options");
    HidOpt.add_options()
            ("input-dir",boost::program_options::value< std::vector<std::string> >(&dirs)->required(),"Directory with figure data")
            ;
    boost::program_options::options_description AllOpt("All options");
    AllOpt.add(GenOpt).add(HidOpt);
    boost::program_options::positional_options_description pos;
    pos.add("input-dir", -1);
    boost::program_options::variables_map vm;
    try 
        { 
        boost::program_options::store(boost::program_options::command_line_parser(argc, argv).
                options(AllOpt).positional(pos).run(), vm);
        
        if ( vm.count("help")  ) 
            { 
            rad::OptionPrinter::printStandardAppDesc(appName, 
                                                 std::cout, 
                                                 AllOpt, 
                                                 &pos);
            return 0; 
            } 
        boost::program_options::notify(vm);
        }
    catch(boost::program_options::required_option& e)
        {
        rad::OptionPrinter::printStandardAppDesc(appName, 
                                                 std::cout, 
                                                 AllOpt, 
                                                 &pos);
        return 1;
        }
    
    std::list<plot_data> data_list;
    
    try
        {
        for(auto it=dirs.begin();it!=dirs.end();++it)
            {
            if((*it)[it->size()-1]!='/')
                it->push_back('/');
            data_list.push_back(plot_data(*it));
            }
        }
    catch(std::exception& e)
        {
        std::cerr<<"Could not load data: "<<e.what()<<std::endl;
        return 1;
        }
    if(vm.count("information"))
		{
		for(auto it=data_list.begin();it!=data_list.end();++it)
			{
			
			it->print_data_information(std::cout);
			std::cout<<"--------------------------------------------------\n";
			}
		}
    if(vm.count("build"))
		{
		for(auto it=data_list.begin();it!=data_list.end();++it)
			{
			it->build_data_for_gnuplot();
			}
		}
    return 0;
    }
